describe('API Test', () => {
  it('should find all objects with property "Category: Authentication & Authorization"', () => {
    //Endpoint call
    cy.request('https://api.publicapis.org/entries')
      .then((response) => {
        //Read response and filter objects by the property "Category: Authentication & Authorization"
        const authObjects = response.body.entries.filter((entry) => {
          return entry.Category === 'Authentication & Authorization';
        });
        //Compare, count and verify the number of objects where the property above appears
        const count = authObjects.length;
        cy.log(`Found ${count} objects with property "Category: Authentication & Authorization"`);
        //Print found objects to the console
        cy.log(authObjects);
      });
  });
});
