describe('template spec', () => {
  it('passes', () => {
    cy.visit('https://example.cypress.io')
  })
})/// <reference types="Cypress" />

import { inventoryPage } from "../pages/inventoryPage";
import { loginPage } from "../pages/loginPage";

const login = new loginPage
const inventory = new inventoryPage

describe('Sauce UI Tests', () =>{
    beforeEach(() => {
        cy.visit('/');
      });

    it('Should be able to Login successfully with valid credentials', () =>{
        //type valid credentials and click the login button
        login.typeUsername();
        login.typePassword();
        login.clickLoginButton();
        //validate successful login
        inventory.validateSuccessfulLogin();
    })

    it('Should verify that items are sorted by name from A to Z', ()=>{
        //type valid credentials and click the login button
        login.typeUsername();
        login.typePassword();
        login.clickLoginButton();
        //validate successful login
        inventory.validateSuccessfulLogin();
        //validate items are sorted by name from A to Z
        inventory.validateItemsSortedAToZ();
    })

    it('Should verify that items are sorted by name from Z to A', ()=>{
        //type valid credentials and click the login button
        login.typeUsername();
        login.typePassword();
        login.clickLoginButton();
        //validate successful login
        inventory.validateSuccessfulLogin();
        //validate items are sorted by name from Z to A
        inventory.changeAndValidateItemsSortingZtoA();
    })
})