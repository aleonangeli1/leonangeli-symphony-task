export class loginPage{
    typeUsername(){
        cy.get('[data-test="username"]').type('standard_user')
    }

    typePassword(){
        cy.get('[data-test="password"]').type('secret_sauce')
    }

    clickLoginButton(){
        cy.get('[data-test="login-button"]').click()
    }
}