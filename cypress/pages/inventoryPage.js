export class inventoryPage{
    
    validateSuccessfulLogin(){
        cy.url().should('eq', 'https://www.saucedemo.com/inventory.html');
        cy.get('[data-test="product_sort_container"]').should('be.visible')
    }

    validateItemsSortedAToZ(){
        cy.get('[data-test="product_sort_container"]').contains('Name (A to Z)');
    }
    changeAndValidateItemsSortingZtoA(){
        cy.get('[data-test="product_sort_container"]').select(1)
        cy.get('[data-test="product_sort_container"]').contains('Name (Z to A)')
    }
    
}