# Leonangeli Symphony Task

**Symphony Solutions Coding Challenge**

Welcome to the Symphony Solutions Coding Challenge! 
This challenge tests your ability to work with a Cypress test suite.

Getting Started 
Follow these instructions to download the repository, install dependencies, and run the Cypress tests.

Prerequisites :
Before running the tests, make sure you have the following installed on your machine:

-Node.js 
-Cypress

Instructions :
1.Open a terminal or command prompt.

2.Clone the repository: Open a terminal and copy the following command: git clone https://gitlab.com/aleonangeli1/symphonytask.git

3.Navigate to the project directory

4.Install Dependencies using npm-install

5.Configure Cypress Before running the tests, you may want to configure Cypress if needed. Cypress configuration files can be found in the cypress directory.

6.Run Cypress Tests To run the Cypress tests, use the following command: npx cypress open follow the cypress wizard

7.Additionally, I added a reporting library that you can run with a custom command : npm run html-report    . Once that the execution is done, you can look in the repository explorer, open the folder reports and right click the index.html, open in finder and execute it with your desired browser in order to see the execution details.

Test Structure 
This repository implements Page Object pattern The Cypress test suite is organized as follows:

cypress/e2e: Contains the test files. cypress/pages: Contains Page 
Objects used by Cypress. 
cypress/support: Contains support files like 
commands and fixtures.

Contributing If you encounter any issues or would like to contribute to this project, feel free to open an issue or submit a pull request.

Writen and coded by Agustin Leonangeli December, 2023
